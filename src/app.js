import React from "react";
import ReactDOM from "react-dom";
import "./styles/main.scss";
import "./images/favicon.ico";

import Home from "./containers/Home";

ReactDOM.render(<Home/>, document.getElementById("react-root"));