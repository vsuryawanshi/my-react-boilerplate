import React, { Component } from "react";


export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = { counter: 0 };
    }

    componentDidMount() {
        this.interval = setInterval(this.increment.bind(this), 1000);
    }

    increment() {
        this.setState(({ counter }) => {
            return { counter: counter + 1 };
        });
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        const { counter } = this.state;

        return (
            <header>
                <div className="main">Webpack is doing its thing with React and babel</div>
                <div className="sub">{counter}</div>
                <img src={require("../images/cartoon.png")}/>
                <img src={require("../images/conversation.svg")} style={{width:100}}/>
            </header>
        );
    }
}